
//#region [raf]
(function() {
	var lastTime = 0;
	var vendors = ['ms', 'moz', 'webkit', 'o'];
	for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
		window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
		window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
									|| window[vendors[x]+'CancelRequestAnimationFrame'];
	}

	if (!window.requestAnimationFrame)
		window.requestAnimationFrame = function(callback, element) {
			var currTime = new Date().getTime();
			var timeToCall = Math.max(0, 16 - (currTime - lastTime));
			var id = window.setTimeout(function() { callback(currTime + timeToCall); },
				timeToCall);
			lastTime = currTime + timeToCall;
			return id;
		};

	if (!window.cancelAnimationFrame)
		window.cancelAnimationFrame = function(id) {
			clearTimeout(id);
		};
}());
//#endregion [raf]

$(function() {
	'use strict';

	var isMobile, isDesktop,
		szMobile = 'screen and (max-width: 1024px)';

	var $win = $(window),
		$mMenu = $('.mmenu'),
		$mMain = $('.mmain'),
		$mWays = $('.mways'),
		$mPart = $('.mpart'),
		$mLang = $('.mlang'),
		$mMobile = $('.mmobile'),
		$fSearch = $('.fsearch'),
		$overlay = $('#overlay'),
		$mMenuBtn = $('.mmenu_button');

	//#region    [!]----------[   MENU   ]----------[!]

	//#region [mmenu]
	function mMenuHide() {
		overlayHide();
		scrollbarsShow();
		$mMenu.removeClass('_open');
		$mMenuBtn.removeClass('mmenu_button-active');
		$mMain.add($mWays).add($mMobile).removeClass('_show');
	}
	function mMobileCreate() {
		$('.mmobile_box').append(
			$mLang,
			$fSearch,
			$mPart,
			$mMain,
			$mWays
		);
	}
	function mMobileRemove() {
		if (!$('.mmobile_box').is(':empty')) {
			mMenuHide();

			$mMenu.append(
				$mMain,
				$mWays,
				$mPart
			);
			$('.header_logo').after(
				$mLang,
				$fSearch
			);
			$('.mmain_li1-open, .mpart_li1-open')
				.removeClass('mmain_li1-open mpart_li1-open');
		}
	}

	$mMenuBtn.on('click', function() {
		var $this = $(this),
			isActive = $this.hasClass('mmenu_button-active');

		if (isActive) {
			mMenuHide();
		} else {
			overlayShow();

			$('.mmobile').scrollTop(0);

			if (isMobile) {
				scrollbarsHide();
			}
			if ($mMenu.hasClass('_lvl1')) {
				$mMenu.addClass('_open');
			}

			$this.addClass('mmenu_button-active');
			$mMain.add($mWays).add($mMobile).addClass('_show');
		}
	});
	$('.mmain_lnk1-sub').on('click', function() {
		if (isMobile) {
			var $item = $(this).parent();

			if ($item.hasClass('mmain_li1-open')) {
				$item.removeClass('mmain_li1-open');
			} else {
				$('.mmain_li1-open, .mpart_li1-open')
					.removeClass('mmain_li1-open mpart_li1-open');
				$item.addClass('mmain_li1-open');
			}

			return false;
		}
	});
	$('.mpart_lnk1-sub').on('click', function() {
		if (isMobile) {
			var $item = $(this).parent();

			if ($item.hasClass('mpart_li1-open')) {
				$item.removeClass('mpart_li1-open');
			} else {
				$('.mmain_li1-open, .mpart_li1-open')
					.removeClass('mmain_li1-open mpart_li1-open');
				$item.addClass('mpart_li1-open');
			}

			return false;
		}
	});
	//#endregion [mmenu]

	//#endregion [!]----------[   MENU   ]----------[!]

	//#region    [!]----------[   BASE   ]----------[!]

	//#region [jobs]
	var jobsTimeout;
	$('.jobs_item_head').click(function() {
		var $this = $(this);

		clearTimeout(jobsTimeout);
		jobsTimeout = setTimeout(function() {
			$this.parent().toggleClass('_open');
			$this.next().slideToggle(400);
		}, 200);
	});
	//#endregion [jobs]

	//#region [table]
	$('.content table').each(function(index, item) {
		var $this = $(item),
			$prnt = $this.parent();
		if (!$prnt.hasClass('ovflw')) {
			if($this.hasClass('table-theme-default')) {
				$this.wrap('<div class="ovflw _table" />');
			} else {
				$this.wrap('<div class="ovflw _table_default" />');
			}
		}
	});
	//#endregion [table]

	//#region [goods]
	if ($('.goods').length) {
		var goodsAnim = new Animate({
			delay: 200,
			remove: false,
			scrolled: true,
			onScroll: true,
			offset: [0.5, 0],
			animatedClass: '_show',
			target: '[data-animate]'
		});
		goodsAnim.init();
	}
	//#endregion [goods]

	//#region [pvideo]
	if ($('.pvideo').length) {
		$(window).on('resize.pvideo', function() {
			var $this = $(this),
				$size = $('.pvideo_size'),
				w_width = $this.width() - 84,
				w_height = $this.height() - 84,
				h_video = (w_width * 360) / 640;

			if (h_video >= w_height) {
				$size.add('.pvideo_view').addClass('_y');
			} else {
				$size.add('.pvideo_view').removeClass('_y');
			}
		}).trigger('resize.pvideo');

		var fnPopupVideoHide = function() {
			var $popup = $('.pvideo');
			if ($popup.hasClass('_show')) {
				$popup.removeClass('_show');
				$('.pvideo_item').remove();

				setTimeout(function() {
					$popup.addClass('_hide');
					scrollbarsShow();
				}, 600);
			}
		};

		$('.js_pvideo_close').on('click', function() {
			fnPopupVideoHide();
		});

		$(document).on('click', '.js_show_video', function() {
			var $this = $(this),
				src = $this.data('src'),
				img = $this.data('img'),
				url = $this.data('url'),
				code = '';

			if (url && url.indexOf('?autoplay=1') < 0) {
				url += '?autoplay=1';
			}

			if (src) {
				code = '<video class="pvideo_item" src="'+src+'" poster="'+img+'" autoplay controls></video>';
			} else {
				code = '<iframe class="pvideo_item" src="'+url+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
			}

			$('.pvideo').addClass('_show').removeClass('_hide');
			$('.pvideo_view').append(code);
			scrollbarsHide();
		});
	}
	//#endregion [pvideo]

	//#endregion [!]----------[   BASE   ]----------[!]

	//#region    [!]----------[   SHOW   ]----------[!]

	//#region [smain]
	var sMainView = !1,
		sMainInfo = !1,
		sMainVids = document.getElementsByClassName('sMain_item_video');

	function sMainInit() {
		var eInfo = document.getElementsByClassName('sMain_info'),
			eView = document.getElementsByClassName('js_sMain_bg'),
			isManyViews = eView.length > 1,
			cfgView = {
				speed: 400,
				fadeEffect: {
					crossFade: true
				}
			},
			cfgInfo = {
				loop: true,
				speed: 400,
				grabCursor: true,
				slideActiveClass: 'swiper-slide-active sMain_item-anim',
				slideDuplicateActiveClass: 'swiper-slide-active sMain_item-anim',
				autoplay: {
					delay: 6000,
					disableOnInteraction: false
				},
				fadeEffect: {
					crossFade: true
				},
				navigation: {
					prevEl: '.sMain_prev',
					nextEl: '.sMain_next',
					disabledClass: '_disabled'
				},
				pagination: {
					clickable: true,
					el: '.sMain_pages',
					bulletClass: 'swiper-pin',
					bulletActiveClass: '_active',
				}
			};

		if (eInfo.length) {
			if (isMobile) {
				cfgView.loop = true;
			} else {
				cfgView.effect = 'fade';
				cfgInfo.effect = 'fade';
				cfgInfo.on = {
					init: function() {
						$('.facts').addClass('_show');
						$('.sMain_iItem:eq(0)').addClass('sMain_item-anim')
							.siblings().removeClass('sMain_item-anim');
					},
					slideChangeTransitionEnd: function() {
						var index = this.realIndex,
							$slide = $(this.slides[this.activeIndex]);

						$slide.addClass('sMain_item-anim');

						if (isManyViews) {
							sMainView.slideTo(index);
						}
					},
				};

				if (isManyViews) {
					cfgInfo.on.slideChangeTransitionStart = function() {
						var index = this.realIndex,
							$slide = $(sMainView.slides[index]),
							video = $slide.find('.sMain_item_video').get(0);

						videoPlay(video);
					}
				}
			}

			if (sMainView) { sMainView.destroy(); }
			if (sMainInfo) { sMainInfo.destroy(); }

			if (isManyViews) {
				sMainView = new Swiper('.sMain_view', cfgView);

				if (isMobile) {
					cfgInfo.controller = {
						control: sMainView
					}
				}
			}

			sMainInfo = new Swiper('.sMain_info', cfgInfo);
		}
	}

	var resizeVideoOn = !1;

	$(window).on('resize.video', function() {
		if (!resizeVideoOn) {
			resizeVideoOn = true;

			requestAnimationFrame(videoCover);
		}
	}).trigger('resize.video');

	function videoStop() {
		for (var i = 0; i < sMainVids.length; i++) {
			var video = sMainVids[i];

			if (video.currentTime > 0) {
				video.pause();
				video.currentTime = 0;
			}
		}
	}
	function videoCover() {
		var $box = $('.sMain'),
			nativeWidth = 1920,
			nativeHeight = 1080,
			$video = $('.sMain_item_video'),
			parentWidth = $box.outerWidth(),
			parentHeight = $box.outerHeight(),
			widthScaleFactor = parentWidth / nativeWidth,
			heightScaleFactor = parentHeight / nativeHeight;

		if (widthScaleFactor > heightScaleFactor) {
			$video.css({
				height: 'auto',
				width: parentWidth
			});
		} else {
			$video.css({
				width: 'auto',
				height: parentHeight
			});
		}

		resizeVideoOn = false;
	}
	function videoPlay(video, isInit) {
		if (!isInit) {
			videoStop();
		}

		if (video && video.paused) {
			var playPromise = video.play();

			if (playPromise !== undefined) {
				playPromise.then(function() {
					//console.log('Video playback started ;)');
				}).catch(function(error) {
					//console.log('Video playback failed ;(');
				});
			}
		}
	}
	//#endregion [smain]

	//#region [sgrid]
	var sGrid = !1,
		$lGrid = $('.lGrid');

	function sGridCreate() {
		if (!sGrid && $lGrid.length) {
			sGrid = new Swiper('.lGrid', {
				loop: true,
				grabCursor: true,
				autoplay: {
					delay: 6000,
					disableOnInteraction: false
				},
				pagination: {
					clickable: true,
					el: '.lGrid_pages',
					bulletClass: 'swiper-pin',
					bulletActiveClass: '_active',
				}
			});
		}
	}
	function sGridRemove() {
		if (sGrid) {
			sGrid.destroy();
			sGrid = false;
		}
	}
	//#endregion [sgrid]

	//#region [shome]
	new Swiper('.sHome_view', {
		loop: true,
		speed: 400,
		effect: 'fade',
		grabCursor: true,
		slideActiveClass: 'swiper-slide-active sHome_item-anim',
		slideDuplicateActiveClass: 'swiper-slide-active sHome_item-anim',
		autoplay: {
			delay: 6000,
			disableOnInteraction: false
		},
		fadeEffect: {
			crossFade: true
		},
		navigation: {
			prevEl: '.sHome_prev',
			nextEl: '.sHome_next',
			disabledClass: '_disabled'
		},
		pagination: {
			clickable: true,
			el: '.sHome_pages',
			bulletClass: 'swiper-pin',
			bulletActiveClass: '_active',
		}
	});
	//#endregion [shome]

	//#region [simage]
	if ($('.sImage_item').length > 1) {
		var sImageView = !1,
			sImageInfo = !1;

		sImageInfo = new Swiper('.sImage_info', {
			loop: true,
			speed: 400,
			noSwiping: true
		});
		sImageView = new Swiper('.sImage_view', {
			loop: true,
			speed: 400,
			grabCursor: true,
			autoplay: {
				delay: 6000,
				disableOnInteraction: false
			},
			controller: {
				control: sImageInfo
			},
			navigation: {
				prevEl: '.sImage_prev',
				nextEl: '.sImage_next',
				disabledClass: '_disabled'
			},
			pagination: {
				type: 'fraction',
				el: '.sImage_pages'
			}
		});
	}
	//#endregion [simage]

	//#endregion [!]----------[   SHOW   ]----------[!]

	//#region    [!]----------[ FORM CONF ]----------[!]

	//#region [fn]
	function fnFormReset($form) {
		$form[0].reset();

		$form.find('.eFile_value').html('');
		$form.find('input').iCheck('update');
		$form.find('select').selectOrDie('update');
		$form.find('._error, ._valid, .eFile').removeClass('_error _valid');
	}
	function fnFormSubmit($form, funcs) {
		var url = $form.attr('action'),
			enctype = $form.attr('enctype'),
			$button = $form.find(':submit'),
			ajaxConfig = {},
			data;

		if (funcs.before) {
			funcs.before();
		}

		$lLoader.fadeIn(400);
		$button.prop('disabled', true);

		if (enctype !== 'multipart/form-data') {
			data = $form.serialize();
		} else {
			data = new FormData($form[0]);

			ajaxConfig = {
				cache: false,
				timeout: 600000,
				processData: false,
				contentType: false
			};
		}

		$.extend(ajaxConfig, {
			url: url,
			data: data,
			type: 'post',
			dataType: 'json',
			error: function(o) {
				if (funcs.fail) {
					funcs.fail(o);
				}
			},
			success: function(o) {
				if (funcs.done) {
					funcs.done(o);
				}
			},
			complete: function() {
				$lLoader.fadeOut(400);
				$button.prop('disabled', false);
			}
		});

		$.ajax(ajaxConfig);
	}
	function fnFormValidate(form, funcs) {
		var $form = $(form);

		if ($form.length) {
			$form.validate({
				submitHandler: function() {
					fnFormSubmit($form, funcs);
				},
				errorPlacement: function() {
					return false;
				}
			});
		}
	}
	//#endregion [fn]

	//#region [config]
	$.validator.setDefaults({
		focusInvalid: false,
		errorClass: '_error',
		validClass: '_valid',
		highlight: function(item, errorClass, validClass) {
			inputHighlight(this, item, errorClass, validClass);
		},
		unhighlight: function(item, errorClass, validClass) {
			inputHighlight(this, item, validClass, errorClass);
		}
	});

	$.validator.addMethod('rule_ltrs', function(value, element) {
		return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z\s]+$/i.test(value);
	}, 'Only letters are allowed');
	$.validator.addMethod('rule_alpn', function(value, element) {
		return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z\d\s\-]+$/i.test(value);
	}, 'Only letters, nembers and punctuation are allowed');
	$.validator.addMethod('rule_phone', function(value, element) {
		return this.optional(element) || /^\+?(\d{3})\s\(\d{2}\)\s(\d{3}\s\d{2}\s\d{2})$/i.test(value);
		//return this.optional(element) || /^\+?(\d{1,3})\s?\(?\d{2,4}\)?\s?(\d{3}(-|\s)?(\d{3}|\d{2}(-|\s)?\d{2}))$/i.test(value);
	}, 'Incorrect phone');
	$.validator.addMethod('rule_file', function(value, element, param) {
		param = typeof param === 'string' ? param.replace(/,/g, '|') : 'png|jpe?g|zip';
		return this.optional(element) || value.match(new RegExp('\\.(' + param + ')$', 'i'));
	}, 'Please enter a value with a valid extension.');

	$.validator.addClassRules('v_dgts', {digits: true});
	$.validator.addClassRules('v_file', {rule_file: true});
	$.validator.addClassRules('v_ltrs', {rule_ltrs: true});
	$.validator.addClassRules('v_alpn', {rule_alpn: true});
	$.validator.addClassRules('v_phone', {rule_phone: true});

	function inputHighlight(o, item, addClass, removeClass) {
		var type = item.type,
			$this = $(item),
			$prnt = $this;

		if (type === 'file') {
			$prnt = $this.closest('.eFile');
		} else
		if (type === 'select-one') {
			$prnt = $this.closest('.sod_select');
		} else
		if (type === 'radio' || type === 'checkbox') {
			$prnt = o.findByName(item.name).closest('.radio, .checkbox');
		}

		$this.add($prnt).addClass(addClass).removeClass(removeClass);
	}
	//#endregion [config]

	//#endregion [!]----------[ FORM CONF ]----------[!]

	//#region    [!]----------[ FORM FLDS ]----------[!]
	$('.form_captcha_refresh').on('click', reloadCaptcha);

	//#region [file]
	$('.js_file').on('change', function() {
		var $this = $(this),
			value = $this.val(),
			$value = $this.next(),
			filesSum = this.files.length;

		if (filesSum > 1) {
			$value.addClass('_many').html(filesSum);
		} else {
			$value.removeClass('_many').html(
				value.replace(/^.*[\\\/]/, '')
			);
		}
	});
	//#endregion [file]

	//#region [select]
	$('select').selectOrDie({
		onChange: function() {
			$(this).valid();
		}
	});
	$('form').on('reset', function() {
		$('select').selectOrDie('update');
	});
	//#endregion [select]

	//#endregion [!]----------[ FORM FLDS ]----------[!]

	//#region    [!]----------[ FORM LIST ]----------[!]

	//#region [fBeep]
	fnFormValidate('#fBeep', {
		done: function(data) {
			reloadCaptcha();

			if (data.return !== 'OK') {
				var err = false;

				$.each(data, function(n, name) {
					err = true;

					$('#fBeep [name="'+ name +'"]').addClass('_error')
						.parent().addClass('_error');
				});
			} else {
				ga('set', 'page', '/contcts/thankyou/');
				ga('send', 'pageview');

				fnFormReset($('#fBeep'));
				$('.window[data-name="send"]').arcticmodal();
			}
		}
	});
	//#endregion [fBeep]

	//#region [fOrder]
	fnFormValidate('#fOrder', {
		done: function() {
			reloadCaptcha();

			if (data.return === 'OK') {
				fnFormReset($('#fOrder'));
				$('.window[data-name="send"]').arcticmodal();
			}
		}
	});
	//#endregion [fOrder]

	//#endregion [!]----------[ FORM LIST ]----------[!]

	//#region    [!]----------[ RESPONSE ]----------[!]
	(function() {
		var resizeOn = !1;

		$win.on('isMobile', function() {
			sMainInit();

			sGridCreate();
			mMobileCreate();
		});
		$win.on('isDesktop', function() {
			sMainInit();

			sGridRemove();
			mMobileRemove();
		});
		$win.on('resize.check', function() {
			if (!resizeOn) {
				resizeOn = true;

				requestAnimationFrame(function() {
					checkScreenSize();
				});
			}
		});

		checkScreenSize();

		function checkScreenSize() {
			if (window.matchMedia(szMobile).matches) {
				if (!isMobile) {
					isMobile = true;
					isDesktop = false;

					$win.trigger('isMobile');
				}
			} else
			if (!isDesktop) {
				isMobile = false;
				isDesktop = true;

				$win.trigger('isDesktop');
			}

			resizeOn = !1;
		}
	}());
	//#endregion [!]----------[ RESPONSE ]----------[!]

	//#region    [!]----------[ FUNCTION ]----------[!]
	function overlayShow() {
		$overlay.fadeIn(400);
	}
	function overlayHide() {
		$overlay.fadeOut(400);
	}
	function scrollbarsHide() {
		$('html').addClass('no_scrollbars');
	}
	function scrollbarsShow() {
		$('html').removeClass('no_scrollbars');
	}
	//#endregion [!]----------[ FUNCTION ]----------[!]



	/*[==> OLD SCRIPTS <==]*/

	//#region [===GROUP]

	//#region [=BLOCK]

	//#region [News block]
	$('.news-block .link-more').on('click', function() {
		$('.news-block .row:not(:first)').slideToggle();
		$(this).toggleClass('active');
		return false;
	});
	//#endregion [News block]

	//#endregion [=BLOCK]

	//#endregion [===GROUP]

	//#region [===SINGLE]

	//#region [Clear]
	$('p:empty').remove();
	//#endregion [Clear]

	//#region [fsearch]
	if ($('.fsearch').length) {
		var $fSearch = $('.fsearch'),
			$fSearchBtn = $('.fsearch_button'),
			fSearchHide = function() {
				$fSearch.removeClass('fsearch-open');
			};

		$('.fsearch_hide').on('click', fSearchHide);

		$fSearchBtn.on('click', function() {
			var $form = $('.fsearch'),
				$input = $('.fsearch_input');

			if (!$form.hasClass('fsearch-open')) {
				$input[0].focus();
				$form.addClass('fsearch-open');

				return false;
			}
		});

		$(document).on('click', function(e) {
			if ($(e.target).closest('.fsearch').length === 0) {
				fSearchHide();
			}
		});
	}
	//#endregion [fsearch]

	//#region [Window]
	$('[data-window]').click(function() {
		var name = $(this).data('window');
		$('.window[data-name="'+name+'"]').arcticmodal();
	});
	//#endregion [Window]

	//#region [News box]
	if ($('.news-box').length) {
		var row = $('.news-box .row:last'),
			cell = row.find('.item');
		if (cell.length === 1) {
			row.append('<div class="cell" />');
			row.append('<div class="cell" />');
		}
		if (cell.length === 2) {
			row.append('<div class="cell" />');
		}
	}
	//#endregion [News box]

	//#region [Scroll up]
	$(window).on('scroll', function() {
		var y = $(window).scrollTop(),
			$button = $('#scroll-up');
		if (y >= 100) {
			$button.removeClass('_hide');
		} else {
			$button.addClass('_hide');
		}
	});
	$('#scroll-up').on('click', function() {
		$('html, body').animate({scrollTop: 0}, 600);
		return false;
	});
	//#endregion [Scroll up]

	//#endregion [===SINGLE]

	//#region [===FORM]
	function reloadCaptcha() {
		$.getJSON('/reload_captcha.php', function(data) {
			$('#captcha_img').attr('src','/bitrix/tools/captcha.php?captcha_sid='+ data);
			$('#captcha_sid').val(data);
			$('#captcha_word').val('');
		});

		return false;
	};
	//#endregion [===FORM]

});

$(window).load(function() {
	$('#preloader').fadeOut(400);
});