
$(function() {
	'use strict';
	
	//#region [goods]
	if ($('.goods').length) {
		var goodsAnim = new Animate({
			delay: 200,
			remove: false,
			scrolled: true,
			onScroll: true,
			offset: [0.5, 0],
			animatedClass: '_show',
			target: '[data-animate]'
		});
		goodsAnim.init();
	}
	//#endregion [goods]

	//#region [mmain]
	var menuMainAnim,
		menuMainSubmenuTimeout;

	if (!$('.mmain').hasClass('_inline')) {
		menuMainAnim = 
			new TimelineMax({paused: true}).add([
				TweenMax.to('.mmain_top', 0.4, {y: '0%', ease: Sine.easeInOut}),
				TweenMax.to('#overlay', 0.6, {opacity: 1, display: 'block', ease: Sine.easeInOut}),
				TweenMax.to('.mmain_bot', 0.6, {y: '100%', bottom: -110, delay: 0.2, ease: Sine.easeInOut})
			]);
	} else {
		menuMainAnim = 
			new TimelineMax({paused: true}).add([
				TweenMax.to('.mmain_top', 0.4, {y: '0%', ease: Sine.easeInOut}),
				TweenMax.to('.mmain_bot', 0.6, {y: '100%',delay: 0.2, ease: Sine.easeInOut}),
				TweenMax.to('#overlay', 0.6, {opacity: 1, display: 'block', ease: Sine.easeInOut})
			]);
		
		var menuMainSubmenuHide = function() {
			new TimelineMax({
				onComplete: function() {
					$('.mmain_sub_li').removeClass('_open');
				}
			}).add([
				TweenMax.to('.mmain_sub', 0.4, {top: -15, ease: Cubic.easeInOut}),
				TweenMax.to('.mmain_top', 0.4, {paddingBottom: 0, ease: Cubic.easeInOut})
			]);
		};
		var menuMainSubmenuShow = function($menu) {
			var $item = $menu.parent(),
				index = $item.index() + 1;
	
			new TimelineMax({
				onComplete: function() {
					$('.mmain_sub_li').removeClass('_open');
					$item.addClass('_open');
				}
			}).add([
				TweenMax.to('.mmain_sub', 0.4, {top: -15, ease: Cubic.easeInOut}),
				TweenMax.to('.mmain_top', 0.4, {paddingBottom: 115, ease: Cubic.easeInOut}),
				TweenMax.to('.mmain_sub_li:nth-child('+index+') > .mmain_sub', 0.4, {top: 100, ease: Cubic.easeInOut})
			]);
		};

		$('.mmain_sub').each(function() {
			var $item = $(this).parent();
			$item.addClass('mmain_sub_li')
				 .append('<div class="mmain_sub_close" />');
		});

		$('.mmain_sub_li')
			.on('mouseenter', function() {
				if (!$('.mmain').hasClass('_open')) {
					var $menu = $(this).children('.mmain_sub');
	
					if ($('.mmain_sub_li._open').length) {
						clearTimeout(menuMainSubmenuTimeout);
						menuMainSubmenuTimeout = setTimeout(function() {
							menuMainSubmenuShow($menu);
						}, 150);
					} else {
						menuMainSubmenuShow($menu);
					}
				}
			}).on('mouseleave', function() {
				if (!$('.mmain').hasClass('_open')) {
					if ($('.mmain_sub_li._open').length) {
						clearTimeout(menuMainSubmenuTimeout);
						menuMainSubmenuTimeout = setTimeout(function() {
							menuMainSubmenuHide();
						}, 150);
					} else {
						menuMainSubmenuHide();
					}
				}
			});
			
		$(document).on('click', function(e) {
			if ($(e.target).closest('.mmain_wrap').length === 0) {
				menuMainSubmenuHide();
			}
		});
		
		$('nav.main').on('mouseleave', function() {
			menuMainSubmenuHide();
		});
		
		$(document).on('mousedown mouseup click', '.mmain_sub_close', function() {
			menuMainSubmenuHide();
		});
		
		$(document).on('mousedown mouseup click', '.mmain_sub_li > .mmain_top_link', function(e) {
			if (!$('.mmain').hasClass('_open')) {
				e.preventDefault();
				return false;
			}
		});
	}

	$('.mmain_button').on('click', function() {
		var $this = $(this);

		if ($this.hasClass('_active')) {
			$('.mmain_button').removeClass('_active');
			$('.mmain').removeClass('_open');
			menuMainAnim.reverse();
		} else {
			$('.mmain_button').addClass('_active');
			$('.mmain').addClass('_open');
			menuMainAnim.play();
		}
	});

	$(document).on('click', function(e) {
		if ($(e.target).closest('.mmain, .mmain_button').length === 0) {
			$('.mmain_button').removeClass('_active');
			$('.mmain').addClass('_open');
			menuMainAnim.reverse();
		}
	});
	//#endregion [mmain]

	//#region [smain]
	(function() {
		var tmShowMainInitA,
			tmShowMainInitB,
			tmShowMainActiveA,
			tmShowMainActiveB,
			videos = document.getElementsByClassName('smain_video');

		$('.smain_view')
			.on('init', function() {
				$('.slick-current').addClass('smain-active');

				$('.facts').addClass('_show');
				$('.smain').addClass('smain-init');
				/*tmShowMainInitA = setTimeout(function() {
					$('.facts').addClass('_show');
		
					tmShowMainInitB = setTimeout(function() {
						$('.smain').addClass('smain-init');
						clearTimeout(tmShowMainInitB);
					}, 600);
				}, 200);*/
			})
			.slick({
				//fade: true,
				speed: 800,
				//autoplay: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				pauseOnHover: false,
				pauseOnFocus: false,
				autoplaySpeed: 8000
			})
			.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
				showMainItemShow($(slick.$slides[nextSlide]));
			});

		$(window).on('resize.video', function() {
			showMainVideoCover();
		});

		function showMainVideoStop() {
			for (var i = 0; i < videos.length; i++) {
				var video = videos[i];
				
				if (video.currentTime > 0) {
					video.pause();
					video.currentTime = 0;
				}
			}
		}
		function showMainVideoCover() {
			var nativeWidth = 1920,
				nativeHeight = 1080,
				$box = $('.smain_size'),
				parentWidth = $box.outerWidth(),
				parentHeight = $box.outerHeight(),
				widthScaleFactor = parentWidth / nativeWidth,
				heightScaleFactor = parentHeight / nativeHeight;

			if (widthScaleFactor > heightScaleFactor) {
				$('.smain_video').css({
					height: 'auto',
					width: parentWidth
				});
			} else {
				$('.smain_video').css({
					width: 'auto',
					height: parentHeight
				});
			}
		}
		function showMainItemShow($item) {
			showMainVideoStop();
				showMainVideoPlay($item);
				$item.addClass('smain-active')
					 .siblings().removeClass('smain-active smain-finish');
					$item.addClass('smain-finish');
			
			/*tmShowMainActiveA = setTimeout(function() {
				clearTimeout(tmShowMainActiveA);
				
				showMainVideoPlay($item);
				$item.addClass('smain-active')
					 .siblings().removeClass('smain-active smain-finish');
					
				tmShowMainActiveB = setTimeout(function() {
					clearTimeout(tmShowMainActiveB);
					$item.addClass('smain-finish');
				}, 1800);
			}, 600);*/
		}
		function showMainVideoPlay($item) {
			var $this = $item.find('.smain_video')[0];
			if ($this) {
				$this.play();
			}
		}
	})();
	//#endregion [smain]

	//#region [pvideo]
	if ($('.pvideo').length) {
		$(window).on('resize.pvideo', function() {
			var $this = $(this),
				w_width = $this.width() - 84,
				w_height = $this.height() - 84,
				$video = $('.pvideo_size'),
				h_video = (w_width * 360) / 640;
			
			if (h_video >= w_height) {
				$video.add('.pvideo_view').addClass('_y');
			} else {
				$video.add('.pvideo_view').removeClass('_y');
			}
		}).trigger('resize.pvideo');

		var fnPopupVideoHide = function() {
			var $popup = $('.pvideo');
			if ($popup.hasClass('_show')) {
				$popup.removeClass('_show');
				$('.pvideo_item').remove();

				setTimeout(function() {
					$popup.addClass('_hide');
					fnShowScroll();
				}, 600);
			}
		};

		$('.js_pvideo_close').on('click', function() {
			fnPopupVideoHide();
		});

		$(document).on('click', '.js_show_video', function() {
			var $this = $(this),
				src = $this.data('src'),
				img = $this.data('img'),
				url = $this.data('url'),
				code = '';

			if (url && url.indexOf('?autoplay=1') < 0) {
				url += '?autoplay=1';
			}

			if (src) {
				code = '<video class="pvideo_item" src="'+src+'" poster="'+img+'" autoplay controls></video>';
			} else {
				code = '<iframe class="pvideo_item" src="'+url+'" frameborder="0" allowfullscreen></iframe>';
			}

			$('.pvideo').addClass('_show').removeClass('_hide');
			$('.pvideo_view').append(code);
			fnHideScroll();
		});
	}
	//#endregion [pvideo]

	//#region [functions]
	function fnHideScroll() {
		$('html').addClass('no_scroll');
	}
	function fnShowScroll() {
		$('html').removeClass('no_scroll');
	}
	//#endregion [functions]


	/*[==> OLD SCRIPTS <==]*/


	/*[===GROUP <]*/
	
	/*[=NAV <]*/
	
	/*[Nav part <]*/
	if ($('nav.part').length) {
		var navPartSubmenuShow = function(submenu) {
			var $item = submenu.parent(),
				index = $item.index() + 1;
			TweenMax.to('nav.part, nav.part .menu > ul > li:nth-child('+index+') > .submenu', 0.4, {
					top: 100,
					paddingBottom: 215,
					ease: Cubic.easeInOut,
					onComplete: function() {
						$('nav.part .menu > ul > li').removeClass('open');
						$item.addClass('open');
					}
				}
			);
		};
		
		var navPartSubmenuHide = function() {
			TweenMax.to('nav.part, nav.part .menu > ul > li > .submenu', 0.4, {
					top: -15,
					paddingBottom: 0,
					ease: Cubic.easeInOut,
					onComplete: function() {
						$('nav.part .menu > ul > li').removeClass('open');
					}
				}
			);
		};
		
		var navPartSubmenuHideTimeout;
		$(document).on('mouseover', 'nav.part .menu > ul > li', function() {
			var $item = $(this),
				submenu = $item.children('.submenu');
			clearTimeout(navPartSubmenuHideTimeout);
			if (submenu.length) {
				navPartSubmenuHide();
				navPartSubmenuShow(submenu);
			} else {
				navPartSubmenuHideTimeout = setTimeout(function() {
					navPartSubmenuHide();
				}, 150);
			}
		});
		
		$('nav.part').hover(function() {}, function() {
			navPartSubmenuHide();
		});
		
		$(document).on('mousedown mouseup click', 'nav.part .menu > ul > li > .close', function() {
			navPartSubmenuHide();
		});
		
		$(document).on('mousedown mouseup click', 'nav.part .menu > ul > li.has-submenu > a', function(e) {
			e.preventDefault(); return false;
		});
		
		$('nav.part .menu > ul > li').each(function() {
			if ($(this).children('.submenu').length) {
				$(this).addClass('has-submenu').append('<div class="close" />');
			}
		});
		
		$(document).on('click', function(e) {
			if ($(e.target).closest('nav.part').length === 0) {
				navPartSubmenuHide();
			}
		});
	}
	/*[Nav part >]*/
	
	/*[Nav paginator <]*/
	if ($('nav.paginator').length) {
		$('body').addClass('has-pagination');
	}
	/*[Nav paginator >]*/
	
	/*[=NAV >]*/
	
	/*[=BLOCK <]*/
	
	/*[News block <]*/
	$('.news-block .link-more').on('click', function() {
		$('.news-block .row:not(:first)').slideToggle();
		$(this).toggleClass('active');
		return false;
	});
	/*[News block >]*/
	
	/*[=BLOCK >]*/
	
	/*[=SLIDER <]*/
	
	/*[Main slider <]*/
	if ($('.main-slider').length) {
		$('.main-slider .slides').slick({
			fade: true,
			dots: true,
			arrows: false,
			autoplay: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplaySpeed: 5000
		});
		
		$(window).on('resize', function() {
			var height = 0,
				h_wn = $(window).innerHeight(),
				h_hd = $('header.main').outerHeight();
			
			height = h_wn - h_hd - 23;
			height = (height < 480) ? 480: height;
			height = (height > 640) ? 640: height;
			$('.main-slider .item').css({'height': height});
		}).trigger('resize');
		
		$('.main-slider .prev').click(function() {
			$(this).parents('.main-slider').children('.slides').slick('slickPrev');
		});
		$('.main-slider .next').click(function() {
			$(this).parents('.main-slider').children('.slides').slick('slickNext');
		});
	}
	/*[Main slider >]*/
	
	/*[Image slider <]*/
	if ($('.image-slider').length) {
		$('.image-slider .slides')
			.on('init', function(slick) {
				var slider = $(slick.$slider).parents('.image-slider'),
					counter = slider.find('.pager .counter'),
					current_slide = slick.currentSlide,
					slides_count = slick.slideCount;
				counter.html((current_slide + 1)+'/'+slides_count);
			})
			.slick({
				fade: true,
				arrows: false,
				slidesToShow: 1,
				draggable: false,
				slidesToScroll: 1
			})
			.on('afterChange', function(slick) {
				var slider = $(slick.$slider).parents('.image-slider'),
					counter = slider.find('.pager .counter'),
					current_slide = slick.currentSlide,
					slides_count = slick.slideCount;
				counter.html((current_slide + 1)+'/'+slides_count);
			});

		$('.image-slider .item .image').click(function() {
			$(this).parents('.image-slider').find('.slides').slick('slickNext');
		});
		
		$('.image-slider .pager .prev').click(function() {
			$(this).parents('.image-slider').find('.slides').slick('slickPrev');
		});
		
		$('.image-slider .pager .next').click(function() {
			$(this).parents('.image-slider').find('.slides').slick('slickNext');
		});
	}
	/*[Image slider >]*/
	
	/*[=SLIDER >]*/
	
	/*[===GROUP >]*/
	
	/*[===SINGLE <]*/
	
	/*[Clear <]*/
	$('p:empty').remove();
	/*[Clear >]*/
	
	/*[Search <]*/
	if ($('.fsearch').length) {
		var searchTween = TweenMax.from('.fsearch_view', 0.2, {
			width: 0,
			paused: true,
			paddingRight: 0,
			ease: Linear.easeNone,
			onComplete: function() {
				$('.fsearch_button').attr('type', 'submit');
			}
		});
		
		$('.fsearch_hide').click(function() {
			searchTween.reverse();
			$('.fsearch').removeClass('_open');
			$('.fsearch_button').attr('type', 'button');
		});
		
		$('.fsearch_button').click(function() {
			var $form = $('.fsearch'),
				$input = $('.fsearch_input');
			if (!$form.hasClass('_open')) {
				$input.focus();
				searchTween.play();
				$form.addClass('_open');
			}
		});
		
		$(document).on('click', function(e) {
			if ($(e.target).closest('.fsearch').length === 0) {
				searchTween.reverse();
				$('.fsearch').removeClass('_open');
				$('.fsearch_button').attr('type', 'button');
			}
		});
	}
	/*[Search >]*/
	
	/*[Window <]*/
	$('[data-window]').click(function() {
		var name = $(this).data('window');
		$('.window[data-name="'+name+'"]').arcticmodal();
	});
	/*[Window >]*/
	
	/*[News box <]*/
	if ($('.news-box').length) {
		var row = $('.news-box .row:last'),
			cell = row.find('.item');
		if (cell.length === 1) {
			row.append('<div class="cell" />');
			row.append('<div class="cell" />');
		}
		if (cell.length === 2) {
			row.append('<div class="cell" />');
		}
	}
	/*[News box >]*/
	
	/*[Scroll up <]*/
	$(window).on('scroll', function() {
		var y = $(window).scrollTop(),
			$button = $('#scroll-up');
		if (y >= 100) {
			$button.removeClass('_hide');
		} else {
			$button.addClass('_hide');
		}
	});
	$('#scroll-up').on('click', function() {
		$('html, body').animate({scrollTop: 0}, 600);
		return false;
	});
	/*[Scroll up >]*/
	
	/*[Vacancy box <]*/
	$('.vacancy-box .item .title').click(function() {
		var item = $(this).parents('.item');
		item.find('.data').slideToggle();
		item.toggleClass('active');
	});
	/*[Vacancy box >]*/
	
	/*[===SINGLE >]*/
	
	/*[===FORM <]*/
	
	/*[Form reset <]*/
	var formReset = function(form) {
		$(form).validate().resetForm();
		$(form).find('input[type="text"], textarea').val('');
		$(form).find('select option:first').prop('selected', true);
		$(form).find('select').selectOrDie('update');
	};
	
	$('button:reset').on('click', function() {
		var form = $(this).parents('form'); formReset(form);
	});
	/*[Form reset >]*/
	
	/*[Form field <]*/
	$('select').selectOrDie();
	/*[Form field >]*/
	
	/*[Form validation <]*/
	if ($.validator) {
	
	/*[Class rules <]*/
	$.validator.addClassRules('_date', {date: true});
	$.validator.addClassRules('_email', {email: true});
	$.validator.addClassRules('_phone', {phone: true});
	$.validator.addClassRules('_digits', {digits: true});
	$.validator.addClassRules('_letters', {letters_only: true});
	$.validator.addClassRules('_letters_digits', {letters_digits: true});
	/*[Class rules >]*/
	
	/*[Feedback form <]*/
	if ($('#feedback-form').length) {
		$('#feedback-form').validate({
			submitHandler: function() {
				$.ajax({
					type: 'post',
					url: $('#feedback-form').attr('action') || '#',
					data: $('#feedback-form').serialize(),
					beforeSend: function() {
					},
					success: function() {
						formReset('#feedback-form');
						$('.window[data-name="send"]').arcticmodal();
					},
					error: function() {
					}
				});
			},
			errorPlacement: function() {
				return false;
			}
		});
	}
	/*[Feedback form >]*/
	
	}
	/*[Form validation >]*/
	
	/*[===FORM >]*/
	
});

$(window).load(function() {
	TweenMax.to('#preloader', 0.6, {opacity: 0, display: 'none'});
});